import Vue from 'vue'
import Vuex from 'vuex'
import colors from './modules/colors'

Vue.use(Vuex)

// export const ACTION_TYPES = {
//   SET_COLOR: 'SET_COLOR'
// }

export default new Vuex.Store({
  modules: {
    colors
  }
  // state: {
  //   color: 'darken'
  // },

  // getters: {
  //   getColor(state) {
  //     return state.color
  //   }
  // },

  // mutations: {
  //   [ACTION_TYPES.SET_COLOR](state, color) {
  //     state.color = color
  //   }
  // },

  // actions: {
  //   setColor({ commit }, color) {
  //     commit(ACTION_TYPES.SET_COLOR, color)
  //   }
  // }
})
