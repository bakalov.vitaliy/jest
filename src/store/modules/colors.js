export default {
  state: {
    color: 'darken'
  },

  getters: {
    getColor(state) {
      return state.color
    }
  },

  mutations: {
    setColor(state, color) {
      state.color = color
    }
  },

  actions: {
    setColor({ commit }, color) {
      commit('setColor', color)
    }
  }
}
