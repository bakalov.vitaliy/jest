import Vue from 'vue'
import VueRouter from 'vue-router'

const UsersList = () => import('@/views/users')
const NewUser = () => import('@/views/new-user')
const EditUser = () => import('@/views/edit-user')

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    { path: '/', redirect: { name: 'users' } },
    { path: '/users', name: 'users', component: UsersList },
    { path: '/users/new', name: 'new-user', component: NewUser },
    { path: '/users/edit/:id', name: 'edit-user', component: EditUser }
  ]
})

export default router
