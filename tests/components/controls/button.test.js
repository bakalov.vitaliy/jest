import { render, screen } from '@testing-library/vue'
import MhButton from '@/components/controls/button.vue'

test('Render button with text', () => {
  const options = {
    slots: {
      default: 'Click Me'
    }
  }

  render(MhButton, options)
  console.dir(screen)

  screen.getByText('Click Me')
})
